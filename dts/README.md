# Quad-Devbox for DTS(Data Science)
Quad Dev VM for Data Science

##### Introduction:
Quad DevBox has build to do all the development task and test it on VM, which is configured similar to our STG and PROD environment. Goal is to have same or very close to same Dev environment as STG and PROD  

Below are the tools we have used to setup the DevBox:

1. Puppet
2. Librarian-puppet
3. Hiera
4. VirtualBox
5. Vagrant

