node default {
  $utils = ['vim-enhanced','nmap', 'httpd','git','git-svn']
   package { $utils:
     ensure => 'installed',
   }
   hiera_include('classes', [])
   #  include python
   #class { 'dts':
   #dts_virtualenv_name => 'dts'
   #}
}

