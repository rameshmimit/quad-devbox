class quad {
  $utils = ['nmap', 'vim-enhanced']
  package { $utils:
    ensure =>  'installed',
  }
  class vcs {
    $vcs = ['git','git-svn']
    case $operatingsystem {
      CentOS: {
        package { $vcs:
          ensure => 'installed',
        }
      }
    }
  }
}
include quad
