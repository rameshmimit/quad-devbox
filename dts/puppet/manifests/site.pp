#import 'test.pp'
#import 'nodes.pp'
# set up a global path, has to be done here
# to make sure it is in global scope
Exec { path => '/sbin:/bin:/usr/sbin:/usr/bin' }
Cron { environment => 'PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin' }

#Set of stages to use for resources that need to be used explictly before or
#  after the main puppet run
stage {'pre':}
stage {'post':}
Stage['pre'] -> Stage['main'] -> Stage['post']
# Explicit decleration of for allowing/disallowing virtual packages
if versioncmp($::puppetversion, '3.6.0') >= 0 {
  Package {
    allow_virtual => false,
  }
}
hiera_include('classes', [])
