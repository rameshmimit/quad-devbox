#node default {
  #$utils = ['vim-enhanced','nmap', 'httpd','git','git-svn']
  # package { $utils
  #   ensure => 'installed,'
   
#package { "screen": ensure => "installed" }
#}
node default {
  $env_settings = join(join_keys_to_values({
    'hostname' => $hostname,
    'role' => $role,
    'domain' => $domain,
    'environment' => $environment,
    'operatingsystem' => $operatingsystem,
    'operatingsystemmajrelease' => $operatingsystemmajrelease,
    'osfamily' => $osfamily,
  }, ' => '), ",\n")

  notify{'facter environment settings':
    message => "FACTER SETTINGS: {\n${env_settings}\n}"
  }
  hiera_include('classes', [])
}

